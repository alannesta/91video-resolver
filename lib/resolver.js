var webPage = require('webpage');
var page = webPage.create();
var system = require('system');
var args = system.args;
var DEFAULT_HOST = '91porn.com';

var host = DEFAULT_HOST;

page.settings.userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36';

page.settings.javascriptEnabled = false;
page.settings.loadImages = false;

if (args.length === 2) {

	page.open(args[1], function(status) {
		if (status === 'fail') {
			system.stdout.writeLine('Fail to open movie page, exiting: 1');
			phantom.exit(1);
		}

		var url = extractSrc(page.content);
		if (url.length > 0) {
			system.stderr.writeLine(url);
			phantom.exit(0);
		} else {
			system.stdout.writeLine('Video not found, exiting: 2');
			phantom.exit(2);
		}


		//waitFor(videoPagePresent, 8000, function() {
		//	//system.stdout.writeLine(page.content);
		//	// extract via regex because phantomjs does not support 'video' tag
		//	var url = extractSrc(page.content);
		//	system.stderr.writeLine(url);
		//	phantom.exit(0);
		//
		//});
	});
}
function waitFor(condition, timeout, callback) {
	var startTime = new Date().getTime();
	var checkPass = 0;
	var elapsed = 0;
	var checkInterval = setInterval(function() {
		var now = new Date().getTime();
		elapsed += now - startTime;
		if (elapsed < timeout) {
			system.stdout.writeLine('check now...');
			startTime = now;
			checkPass = page.evaluate(condition);
			if (checkPass) {
				clearInterval(checkInterval);
				callback();
			}
		} else {
			// timeout reached
			clearInterval(checkInterval);
			console.log('timeout reached, exit');
			phantom.exit(2);
		}
	}, 800);
}

function videoPagePresent() {
	return document.querySelectorAll('div#viewvideo').length > 0;
}

function extractSrc(content) {
	var regex = /<source src="(.*?)"/;
	var match = content.match(regex);
	if (match !== null) {
		return match[1];
	}
	return '';
}

