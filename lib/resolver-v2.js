const request = require('request');
const strencode = require('./decoder_v2');

const Resolver = {
    parse: function(url) {
        return new Promise((resolve, reject) => {
            let j = request.jar();
            let cookie = request.cookie('language=cn_CN');
            j.setCookie(cookie, url);

            request({
                url: url,
                jar: j,
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36'
                }
            }, (error, response, body) => {
                if (error) {
                    reject(error);
                }
                if (!error && response.statusCode === 200) {
                    try {
                        resolve(this.extractUrl(body));
                    } catch (err) {
                        console.log(err);
                        console.log('Fail to resolve using resolver lib, fall back to extract from <source src= tag');
                        try {
                            resolve(this.extractUrlFallback(body))
                        } catch (e) {
                            console.log(e);
                            reject(e);
                        }
                    }
                }
            });
        });
    },

    extractUrl: function(content) {
        // const str = `document.write(strencode("Mi52Ey4CehIUZxsgDCY6CBNEG2NSHQ1iBRdqFBUEEy8cAjlJOysMEAY5BA4ISVERUSZmGAUbf04JTi1lLxpZDhUMGGUSXBJpAT5QAAFDBQZ/MRU5BxUyHmc7JTksGhg5bSFzIzwBIDoRMTRkOWYOGxsgBUovDCgIYmdqBgdzOAghRz41MzE3Fyp+cEEsJVtO","bf8eJZ0xN4YZoKw1YvsS6ULTInRlZPtZQhlxwAkiJSoyD0he2b7bIa29G4FPbIlzvHIJqoCPRRimSra00iOmUAVj0cgSOtNK7bBEiRzVAeqUw2CbUdLzbXKf+/83d4m1ktdEivaafLA6","Mi52Ey4CehIUZxsgDCY6CBNEG2NSHQ1iBRdqFBUEEy8cAjlJOysMEAY5BA4ISVERUSZmGAUbf04JTi1lLxpZDhUMGGUSXBJpAT5QAAFDBQZ/MRU5BxUyHmc7JTksGhg5bSFzIzwBIDoRMTRkOWYOGxsgBUovDCgIYmdqBgdzOAghRz41MzE3Fyp+cEEsJVtO"));`;
        const regex = /write\((strencode\(.*?\))/;

        const srcRegex = /src='(.*?)'/;

        const matches = content.match(regex);

        if (matches) {
            try {
                const fnc = Function('strencode', `"use strict";return ${matches[1]}`);
                const evaluated = fnc(strencode);
                console.log(evaluated);
                const urlMatches = evaluated.match(srcRegex);
                if (urlMatches) {
                    return urlMatches[1]
                }
            } catch (err) {
                throw err;
            }
            throw new Error('cannot parse url from page source(regex match failed)');
        }
        throw new Error('cannot parse url from page source(regex match failed)');
    },

    extractUrlFallback(content) {
        const regex = /<source src="(.*?)"/;
        const match = content.match(regex);
        if (match !== null) {
            return match[1];
        }
        throw new Error('could not extract url via <source src=, please check page encoding')
    },

    extractUrlDeprecated: function() {
        const str = `document.write(atob("PHNvdXJjZSBzcmM9J2h0dHA6Ly8xOTIuMjQwLjEyMC4zNC8vbXA0My8zMDcxMzgubXA0P3N0PWc3T1h1aGpuTHJKTGlBOENvQ18zSUEmZT0xNTUyOTg5Mzg5JyB0eXBlPSd2aWRlby9tcDQnPg=="))`;

        const regex = /write\((atob\(.*?\))/;
        return Buffer.from(str.match(regex)[1], 'base64').toString('utf-8');
    }
};

module.exports = Resolver;